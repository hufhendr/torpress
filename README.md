# WordPress in the Tor docker image
# Example usage with docker-compose
### 1. Create tor directory
```
mkdir tor
sudo chown -R 101:101 tor
```
### 2. Create a docker-compose.yml with the following content
```yaml
version: "3"
services:
  tor:
    image: thetorproject/obfs4-bridge
    container_name: tor
    volumes:
       - ./tor:/var/lib/tor
       - ./torrc:/etc/tor/torrc
    restart: always

  db:
    # We use a mariadb image which supports both amd64 & arm64 architecture
    image: mariadb:latest
    container_name: mariadb
    volumes:
      - db_data:/var/lib/mysql
    restart: always
    environment:
      - MYSQL_ROOT_PASSWORD=somewordpress
      - MYSQL_DATABASE=wordpress
      - MYSQL_USER=wordpress
      - MYSQL_PASSWORD=wordpress
    expose:
      - 3306
      - 33060

  wordpress:
    image: wordpress:latest
    container_name: wordpress
    volumes:
      - wp_data:/var/www/html
    ports:
      - 80:80
    restart: always
    environment:
      - WORDPRESS_DB_HOST=db
      - WORDPRESS_DB_USER=wordpress
      - WORDPRESS_DB_PASSWORD=wordpress
      - WORDPRESS_DB_NAME=wordpress
volumes:
  db_data:
  wp_data:

```
### 3. Create the tor config
- torrc documentation https://2019.www.torproject.org/docs/tor-manual.html.en

```
$ nano ./torrc
HiddenServiceDir /var/lib/tor/hs/
HiddenServicePort 80 wordpress:80
```

### 4. Start docker-compose
```
sudo docker-compose up -d
```

### 5. Get the onion address (hostname)
```
sudo cat ./tor/hs/hostname
```
Output: youronionaddress.onion

### 6. Access WordPress over Tor
Download and install Tor Browser from https://www.torproject.org/download/
or https://brave.com

Now WordPress can be accessed at http://youronionaddress.onion from any Tor Browser.

### 7. Be patient
It takes a while for information to spread through the network. Even 2 hours may not be a long time.

### 8. Prepare the site
In the meantime, you can read the logs from the tor container or prepare your new pages. You can uncomment ports and connect to http://localhost  - WordPress but only allow this access from your own local network.

### 9. WordPress documentation
Study the [WordPress documentation](https://wordpress.org/documentation/article/get-started-with-wordpress/#step-four-set-up-wordpress), at least for publishing new posts. 


Thanks to [lu4p](https://github.com/lu4p/tor-docker)