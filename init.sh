#   **********************************************
#   script for WordPress in the Tor docker install
#   begin     : Sun 19 Feb 2023
#   copyright : (c) 2023 Václav Dvorský
#   $Id: init.sh, v1.00 19/02/2023
#   test with tor v0.4.7.10, ARM and x86_64
#   **********************************************
#
#   --------------------------------------------------------------------
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public Licence as published by
#   the Free Software Foundation; either version 2 of the Licence, or
#   (at your option) any later version.
#   --------------------------------------------------------------------

#!/bin/bash
if ! [ $(id -u) = 0 ]; then
    if [ -d KLF ]
    then
        echo "The tor directory already exists"
        exit 0
    fi

    # preparing the environment for the first run
    mkdir -p tor
    sudo chown -R 101:101 tor

  exit 0
fi
    echo "Do not run this script as root"
